freeze;

/////////////////////////////////////////////////////////////////////////
// minimubs.m
/////////////////////////////////////////////////////////////////////////
// Authors: Alexander Kasprzyk and Gary McConnell
/////////////////////////////////////////////////////////////////////////

// The cache of candidates for entries in MUB vectors
cache:=NewStore();

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Given a prime p and integers n, d, returns the set of elements in
// GF((p^n)^2) with norm 1/d.
function get_candidates(p,n,d)
    bool,Scache:=StoreIsDefined(cache,"Scache");
    if not bool then
        Scache:=AssociativeArray(PowerSequence(Integers()));
    end if;
    key:=[Integers() | p,n,d];
    bool,S:=IsDefined(Scache,key);
    if not bool then
        K:=GF((p^n)^2);
        // All-important const: analogue of "square of cosine of angle between
        // unit vectors". Note: free to vary from usual d^-1 but is a norm so
        // in ground field and not equal to 0 or 1 (avoid confusing 'MUBness'
        // with 'orthonormality').
        k_S:=K ! d^-1;
        // Set of candidates for entries in MUB vectors
        S:={K | x : x in K | FrobeniusNorm(x) eq k_S};
        // Update the cache
        Scache[key]:=S;
        StoreSet(cache,"Scache",Scache);
    end if;
    return S;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic IsQuadraticExtension( K::FldFin ) -> BoolElt,RngIntElt,RngIntElt
{True iff the finite field K is equal to a GF((p^n)^2). If true, also returns the prime p and exponent n.}
    bool,p,n2:=IsPrimePower(#K);
    if bool and IsEven(n2) then
        return true,p,n2 div 2;
    end if;
    return false,_,_;
end intrinsic;

intrinsic FrobeniusConjugate( x::FldFinElt ) -> FldFinElt
{The Frobenius conjugate of a field element relative to a quadratic extension of GF(p^n)}
    // Sanity check
    bool,_,n:=IsQuadraticExtension(Parent(x));
    require bool: "Argument must lie in a finite field of size (p^n)^2";
    // Return the conjugate
    return Frobenius(x,n);
end intrinsic;

intrinsic FrobeniusConjugate( S::SeqEnum[FldFinElt] ) -> SeqEnum[FldFinElt]
{The Frobenius conjugate of each field element in S relative to a quadratic extension of GF(p^n)}
    // Sanity check
    require not IsNull(S): "Illegal null sequence";
    bool,_,n:=IsQuadraticExtension(Universe(S));
    require bool: "Universe must be a finite field of size (p^n)^2";
    // Return the sequence of conjugates
    return [Universe(S) | Frobenius(x,n) : x in S];
end intrinsic;

intrinsic FrobeniusNorm( x::FldFinElt ) -> FldFinElt
{The norm from the quadratic extension of GF(p^n) down to the base field GF(p^n)}
    // Sanity check
    bool,_,n:=IsQuadraticExtension(Parent(x));
    require bool: "Argument must lie in a finite field of size (p^n)^2";
    // Return the conjugate of x with itself
    return Frobenius(x,n) * x;
end intrinsic;

intrinsic InnerProduct( v::ModTupFldElt, w::ModTupFldElt ) -> FldFinElt
{The inner product for a pair of elements in a vector space over GF((p^n)^2)}
    // Sanity check
    VS:=Parent(v);
    require VS cmpeq Parent(w): "Arguments must lie in the same vector space";
    bool,_,n:=IsQuadraticExtension(CoefficientRing(VS));
    require bool:
        "Arguments must be elements of a vector space over GF((p^n)^2)";
    // Return the sum of the conjugates of the elements
    return &+[Frobenius(v[i],n) * w[i] : i in [1..Rank(VS)]];
end intrinsic;

intrinsic InnerProduct( v::ModTupFldElt, S::SeqEnum[ModTupFldElt] )
    -> SeqEnum[FldFinElt]
{The inner product for each pair of elements v,w for w in S, where the elements lie in a vector space over GF((p^n)^2)}
    // Sanity check
    require not IsNull(S): "Argument 2: Illegal null sequence";
    VS:=Parent(v);
    require VS cmpeq Universe(S): "Arguments must lie in the same vector space";
    bool,_,n:=IsQuadraticExtension(CoefficientRing(VS));
    require bool:
        "Arguments must be elements of a vector space over GF((p^n)^2)";
    // Return the sum of the conjugates of the elements
    rk:=Rank(VS);
    K:=CoefficientRing(VS);
    return [K | &+[Frobenius(v[i],n) * w[i] : i in [1..rk]] : w in S];
end intrinsic;

intrinsic InnerProduct( S::SeqEnum[ModTupFldElt], w::ModTupFldElt )
    -> SeqEnum[FldFinElt]
{The inner product for each pair of elements v,w for v in S, where the elements lie in a vector space over GF((p^n)^2)}
    // Sanity check
    require not IsNull(S): "Argument 1: Illegal null sequence";
    VS:=Parent(w);
    require VS cmpeq Universe(S): "Arguments must lie in the same vector space";
    bool,_,n:=IsQuadraticExtension(CoefficientRing(VS));
    require bool:
        "Arguments must be elements of a vector space over GF((p^n)^2)";
    // Return the sum of the conjugates of the elements
    rk:=Rank(VS);
    K:=CoefficientRing(VS);
    return [K | &+[Frobenius(v[i],n) * w[i] : i in [1..rk]] : v in S];
end intrinsic;

intrinsic Norm1Vectors( V::ModTupFld ) -> SetEnum[ModTupFldElt]
{All vectors in the vector space V of rank N over GF((p^n)^2) such that the entries have norm 1/N}
    // Sanity check
    K:=CoefficientRing(V);
    bool,p,n:=IsQuadraticExtension(K);
    require bool: "Argument must be defined over GF((p^n)^2)";
    // Calculate the norm-one vectors
    N:=Rank(V);
    S:=get_candidates(p,n,N);
    ChangeUniverse(~S,K);
    N1:={V | &cat[[x : x in I]] : I in CartesianPower(S,N)};
    k_S:=K ! N^-1;
    if k_S eq K ! 1 then
        N1:={V | v : v in N1 | &+[Frobenius(v[i],n) * v[i] : i in [1..N]] eq 1};
    end if;
    return N1;
end intrinsic;

intrinsic Norm1Vectors( V::ModTupFld, d::RngIntElt ) -> SetEnum[ModTupFldElt]
{All vectors in the vector space V of rank N over GF((p^n)^2) such that the first d entries have norm 1/d, plus possible isotropic tail}
    // Sanity check
    K:=CoefficientRing(V);
    bool,p,n:=IsQuadraticExtension(K);
    require bool: "Argument must be defined over GF((p^n)^2)";
    require d gt 0: "Argument 2 must be a positive integer";
    N:=Rank(V);
    require N ge d: "Argument 2 must not exceed the rank of argument 1";
    // Calculate the norm-one vectors
    S:=get_candidates(p,n,d);
    ChangeUniverse(~S,K);
    if N eq d then
        N1:={V | &cat[[x : x in I]] : I in CartesianPower(S,d)};
    else
        // Tail vector space where we search for isotropic vectors
        iV:=CartesianPower(K,N - d);
        // All isotropic vectors (see E Artin 'Geometric Algebra' III.6 for
        // calcs of orders of this set)
        i_vex:={iV | v : v in iV | &+[FrobeniusConjugate(c)*c : c in v] eq 0};
        // First d entries "sq norm 1/d" plus isotropic tail
        N1:={V | &cat[[x : x in J] : J in I] :
                              I in CartesianProduct(CartesianPower(S,d),i_vex)};
    end if;
    k_S:=K ! d^-1;
    if k_S eq K ! 1 then
        N1:={V | v : v in N1 | &+[Frobenius(v[i],n) * v[i] : i in [1..N]] eq 1};
    end if;
    return N1;
end intrinsic;

intrinsic IsMUB( v::ModTupFldElt, w::ModTupFldElt ) -> BoolElt
{}
    // Sanity check
    VS:=Parent(v);
    require VS cmpeq Parent(w): "Arguments must lie in the same vector space";
    K:=CoefficientRing(VS);
    bool,p,n:=IsQuadraticExtension(K);
    require bool:
        "Arguments must be elements of a vector space over GF((p^n)^2)";
    // Fetch the candidates for entries in MUB vectors
    N:=Rank(VS);
    S:=get_candidates(p,n,N);
    ChangeUniverse(~S,K);
    // Are the elements MUB?
    return &+[Frobenius(v[i],n) * w[i] : i in [1..N]] in S;
end intrinsic;

intrinsic IsMUB( v::ModTupFldElt, w::ModTupFldElt, d::RngIntElt ) -> BoolElt
{True iff v and w are MUB to one another}
    // Sanity check
    VS:=Parent(v);
    require VS cmpeq Parent(w): "Arguments must lie in the same vector space";
    K:=CoefficientRing(VS);
    bool,p,n:=IsQuadraticExtension(K);
    require bool:
        "Arguments must be elements of a vector space over GF((p^n)^2)";
    require d gt 0: "Argument 3 must be a positive integer";
    N:=Rank(VS);
    require N ge d: "Argument 3 must not exceed the rank of the vector space";
    // Fetch the candidates for entries in MUB vectors
    S:=get_candidates(p,n,d);
    ChangeUniverse(~S,K);
    // Are the elements MUB?
    return &+[Frobenius(v[i],n) * w[i] : i in [1..N]] in S;
end intrinsic;

intrinsic IsMUB( v::ModTupFldElt, W::SetEnum[ModTupFldElt] ) -> BoolElt
{}
    // Sanity check
    require not IsNull(W): "Argument 2: Illegal null set";
    VS:=Parent(v);
    require VS cmpeq Universe(W): "Arguments must lie in the same vector space";
    K:=CoefficientRing(VS);
    bool,p,n:=IsQuadraticExtension(K);
    require bool:
        "Arguments must be elements of a vector space over GF((p^n)^2)";
    // Fetch the candidates for entries in MUB vectors
    N:=Rank(VS);
    S:=get_candidates(p,n,N);
    ChangeUniverse(~S,K);
    // Are the elements MUB?
    return &and[&+[Frobenius(v[i],n) * w[i] : i in [1..N]] in S : w in W];
end intrinsic;

intrinsic IsMUB( v::ModTupFldElt, W::SetEnum[ModTupFldElt], d::RngIntElt )
    -> BoolElt
{True iff v is MUB to every element of W}
    // Sanity check
    require not IsNull(W): "Argument 2: Illegal null set";
    VS:=Parent(v);
    require VS cmpeq Universe(W): "Arguments must lie in the same vector space";
    K:=CoefficientRing(VS);
    bool,p,n:=IsQuadraticExtension(K);
    require bool:
        "Arguments must be elements of a vector space over GF((p^n)^2)";
    require d gt 0: "Argument 3 must be a positive integer";
    N:=Rank(VS);
    require N ge d: "Argument 3 must not exceed the rank of the vector space";
    // Fetch the candidates for entries in MUB vectors
    S:=get_candidates(p,n,d);
    ChangeUniverse(~S,K);
    // Are the elements MUB?
    return &and[&+[Frobenius(v[i],n) * w[i] : i in [1..N]] in S : w in W];
end intrinsic;

intrinsic IsMUB( V::SetEnum[ModTupFldElt], W::SetEnum[ModTupFldElt] ) -> BoolElt
{}
    // Sanity check
    require not IsNull(V): "Argument 1: Illegal null set";
    require not IsNull(W): "Argument 2: Illegal null set";
    VS:=Universe(V);
    require VS cmpeq Universe(W): "Arguments must lie in the same vector space";
    K:=CoefficientRing(VS);
    bool,p,n:=IsQuadraticExtension(K);
    require bool:
        "Arguments must be elements of a vector space over GF((p^n)^2)";
    // Fetch the candidates for entries in MUB vectors
    N:=Rank(VS);
    S:=get_candidates(p,n,N);
    ChangeUniverse(~S,K);
    // Are the elements MUB?
    return &and[&+[Frobenius(v[i],n) * w[i] : i in [1..N]] in S :
                                                        v in V, w in W];
end intrinsic;

intrinsic IsMUB( V::SetEnum[ModTupFldElt], W::SetEnum[ModTupFldElt],
    d::RngIntElt ) -> BoolElt
{True iff every element of V is MUB to every element of W}
    // Sanity check
    require not IsNull(V): "Argument 1: Illegal null set";
    require not IsNull(W): "Argument 2: Illegal null set";
    VS:=Universe(V);
    require VS cmpeq Universe(W): "Arguments must lie in the same vector space";
    K:=CoefficientRing(VS);
    bool,p,n:=IsQuadraticExtension(K);
    require bool:
        "Arguments must be elements of a vector space over GF((p^n)^2)";
    require d gt 0: "Argument 3 must be a positive integer";
    N:=Rank(VS);
    require N ge d: "Argument 3 must not exceed the rank of the vector space";
    // Fetch the candidates for entries in MUB vectors
    S:=get_candidates(p,n,d);
    ChangeUniverse(~S,K);
    // Are the elements MUB?
    return &and[&+[Frobenius(v[i],n) * w[i] : i in [1..N]] in S :
                                                        v in V, w in W];
end intrinsic;

intrinsic IsOrthogonal( v::ModTupFldElt, W::SetEnum[ModTupFldElt] ) -> BoolElt
{True iff v is orthogonal to every element in W}
    // Sanity check
    require not IsNull(W): "Illegal null set";
    VS:=Parent(v);
    require VS cmpeq Universe(W): "Arguments must lie in the same vector space";
    bool,_,n:=IsQuadraticExtension(CoefficientRing(VS));
    require bool:
        "Arguments must be elements of a vector space over GF((p^n)^2)";
    // Check for orthogonality
    N:=Rank(VS);
    return &and[&+[Frobenius(v[i],n) * w[i] : i in [1..N]] eq 0 : w in W];
end intrinsic;

intrinsic OrthogonalSubset( v::ModTupFldElt, W::SetEnum[ModTupFldElt] )
    -> SetEnum[ModTupFldElt]
{The subset of W containing only elements orthogonal with v}
    // Sanity check
    require not IsNull(W): "Illegal null set";
    VS:=Parent(v);
    require VS cmpeq Universe(W): "Arguments must lie in the same vector space";
    bool,_,n:=IsQuadraticExtension(CoefficientRing(VS));
    require bool:
        "Arguments must be elements of a vector space over GF((p^n)^2)";
    // Check for orthogonality
    N:=Rank(VS);
    return {VS | w : w in W | &+[Frobenius(v[i],n) * w[i] : i in [1..N]] eq 0};
end intrinsic;

intrinsic IsOrthonormal( X::SetEnum[ModTupFldElt] ) -> BoolElt
{True iff X is orthonomal}
    // Sanity check
    require not IsNull(X): "Illegal null set";
    VS:=Universe(X);
    bool,_,n:=IsQuadraticExtension(CoefficientRing(VS));
    require bool:
        "Arguments must be elements of a vector space over GF((p^n)^2)";
    // Check orthonormality
    N:=Rank(VS);
    return &and[&+[Frobenius(x[i],n) * x[i] : i in [1..N]] eq 1 : x in X] and
           &and[&+[Frobenius(x[i],n) * y[i] : i in [1..N]] eq 0 :
                                                       x in X, y in X | x ne y];
end intrinsic;

intrinsic AreOrthononormalMUBs( B::SetEnum[SetEnum[ModTupFldElt]] ) -> BoolElt
{True iff for each X in B, X is orthonormal and, for each Y in B not equal to X, the elements of X and Y are MUB to each other}
    // Sanity check
    require not #B eq 0: "Argument must not be empty";
    d:=#Representative(B);
    require d gt 0: "The bases must be of positive length";
    require &and[#X eq d : X in B]: "The bases must be of the same length";
    N:=Rank(Universe(Representative(B)));
    require N ge d:
        "The bases must not be longer than the rank of the vector space";
    // First check orthonormality
    if not &and[IsOrthonormal(X) : X in B] then
        return false;
    end if;
    // Now check MUBness
    B:=SetToSequence(B);
    return &and[IsMUB(B[i],B[j],d) : j in [i + 1..#B], i in [1..#B - 1]];
end intrinsic;

intrinsic ComputationalBasis( V::ModTupFld, d::RngIntElt )
    -> SetEnum[ModTupFldElt]
{The computational basis of V of length d}
    // Sanity check
    require d gt 0: "Argument 2 must be a positive integer";
    N:=Rank(V);
    require N ge d: "Argument 2 must not exceed the rank of argument 1";
    // Build the basis
    return SequenceToSet(Basis(V)[1..d]);
end intrinsic;
